package com.jamin.demo.nettypool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableAutoConfiguration
public class NettyPoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettyPoolApplication.class, args);
    }

}
