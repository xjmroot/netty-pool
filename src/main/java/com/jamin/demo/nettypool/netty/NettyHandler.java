package com.jamin.demo.nettypool.netty;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyHandler extends ChannelInboundHandlerAdapter {
    private static final Logger log = LoggerFactory.getLogger("netty-demo");

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //消息唯一标识这里演示写为msg-sn,正常流程应由服务端返回,这里解析
        String key = "msg-sn";
        String msgStr = msg.toString();

            try{


                //消息唯一标识这里演示写为test-key,正常流程应由服务端返回,这里解析
                NettyTools.setReceiveMsg(key, msgStr);
            }catch (Exception e){
                log.error("消息转码失败:{} ",msg.toString());
                e.printStackTrace();
            }



    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {

        log.error("异常:{}",cause.getMessage());
        ctx.fireExceptionCaught(cause);
    }

}
