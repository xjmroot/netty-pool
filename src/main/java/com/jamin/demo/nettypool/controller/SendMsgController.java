package com.jamin.demo.nettypool.controller;


import com.jamin.demo.nettypool.service.NettyClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用netty连接池发送消息
 * 异步等待回调结果测试
 *
 * @author : Jamin
 * @date : 2020/04/26 18:11
 */
@RestController
@RequestMapping
public class SendMsgController {
    private static final Logger log = LoggerFactory.getLogger("netty-demo");

    @Autowired
    NettyClientService nettyClientService;

    /**
     * 发送消息到指定tcp服务器
     * @param addr 消息发送地址, 格式 host:port
     * @return String 消息响应结果
     */
    @GetMapping("/netty/send/msg")
    public String index(@RequestParam(name = "addr") String addr){

        if(addr == null || addr.equals("")){
            return "addr 必填";
        }
        String msg = "我是发送消息内容";

        String result = nettyClientService.sendMsg(addr,msg);

        if(result != null){
            return String.format("发送地址:%s,\r\n发送内容:%s\r\n响应内容:%s",addr,msg,result);
        }

        return "服务端无响应";
    }

}