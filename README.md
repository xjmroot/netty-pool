netty所有请求都是异步的，这个项目演示了使用netty自带连接池，异步发送消息，*同步获取返回结果*（利用BlockingQueue回调）

## 程序结构
```
netty-pool
 ├── pom.xml
 └── src
     └── main
         ├── java
         │   └── com.jamin.demo.nettypool
         │                   ├── controller
         │                   │   └── SendMsgController.java  （WEB访问入口）
         │                   ├── netty
         │                   │   ├── NettyHandler.java  (服务端响应处理)
         │                   │   ├── NettyPool.java		（netty连接池）
         │                   │   └── NettyTools.java	（工具）
         │                   ├── NettyPoolApplication.java （程序启动入口）
         │                   └── service
         │                       └── NettyClientService.java （客户端消息发送）
         └── resources
             └── application.properties
```